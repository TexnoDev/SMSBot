## • API Qurulum
* Bu linkdən öz hesabınıza daxil olun : http://my.telegram.org.

* " API development tools " daxil olub lazımlı sahələri doldurun. 

* Platfotma olaraq "Other" seçin. 

*  "api_id" və "api_hash" məlumatlarını kopyalayın ( setup.py üçün )

## • Qurulma qaydası

`$ pkg install git -y

`$ pkg install python -y

`$ pkg update -y

`$ git clone https://gitlab.com/TexnoDev/SMSBot/

`$ cd SMSBot`


* Tələbləri quraşdırma

`$ python3 setup.py -i`

* Kopyaladığınız məlumatları sırasi ilə daxil edin. ( apiID, apiHASH, Telefon nömrənizi )

`$ python3 setup.py -c `

*  Botu Güncəlləmək

`$ python3 setup.py -u`
v